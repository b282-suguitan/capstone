import {useState, useEffect} from 'react';
import AppNavbar from './components/AppNavBar';
import ProductView from './components/ProductView';
import Create from './pages/Create';
import Update from './pages/Update';


import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Admin from './pages/Admin'
import Dashboard from './pages/Dashboard';


import './App.css';

import {Container} from 'react-bootstrap';

import { UserProvider } from './UserContext';

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

function App() {
  


const [user, setUser] = useState({
  id: null,
  isAdmin: null
});

const unsetUser = () => {
  localStorage.clear();
}

useEffect(() => {
  fetch(`${process.env.REACT_APP_URL}/users/details`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(res => res.json())
  .then( data => {
    // Logging the response data for debugging purposes
    console.log(data);

    // user is logged in
    if (typeof data._id !== "undefined") {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      });
    } else {
      // user is logged out or invalid response
      setUser({
        id: null,
        isAdmin: null
      });
    }
  })
  .catch(error => {
    console.error('Error fetching user details:', error);
    // Handle the error gracefully, e.g., setUser to some default state
    setUser({
      id: null,
      isAdmin: null
    });
  });
}, []);

 
  return (
    <>
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
      <AppNavbar />
        <Container>
          <Routes>
            <Route path= "/" element={<Register />}/>
            <Route path= "/login" element={<Login />}/>
            <Route path= "/home" element={<Home />}/>
            <Route path= "/products" element={<Products />}/>
            <Route path= "/products/:productId" element={<ProductView />}/>
            <Route path= "/logout" element={<Logout />}/>
            <Route path= "/*" element={<Error />}/>
            <Route path= "/admin" element={<Admin />}/>
            <Route path= "/dashboard" element={<Dashboard />}/>
            <Route path= "/create" element={<Create />}/>
            <Route path= "/update" element={<Update />}/>


          </Routes>
          
        </Container>
      </Router>
    </UserProvider>  
    </>
  );
}

export default App;



