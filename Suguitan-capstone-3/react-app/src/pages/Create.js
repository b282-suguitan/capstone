import React, { useState, useEffect } from 'react';
import { Button, Container, Col, Row, Form } from 'react-bootstrap'; // Make sure to import Card and Button from react-bootstrap
import 'animate.css';
import '../App.css'
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';

export default function Create() {

  const [productName, setProductName] = useState('');
  const [productDescription, setProductDescription] = useState('');
  const [productPrice, setProductPrice] = useState('');
  const [, setProducts] = useState([]);
  const [isActive, setIsActive] = useState(false);
  const navigate = useNavigate();


    useEffect(() => {
      if( productName !== "" && productDescription!== "" && productPrice !== "") {

          setIsActive(true)
      } else {
          setIsActive(false)
      }
  }, [productName, productDescription, productPrice])

    function createProduct(e) {
      e.preventDefault();


    // Create
    fetch(`${process.env.REACT_APP_URL}/products/create`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        name: productName,
        description: productDescription,
        price: productPrice
      })
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('Product created:', data);
       
        if(data !== "undefined"){

          // Clear input fields
          setProductName("");
          setProductDescription("");
          setProductPrice("");

          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Product Created!',
            showConfirmButton: false,
            timer: 1500
          })

          navigate("/dashboard")

        } else {
          Swal.fire({
            title: "Invalid Input!",
            icon: "error",
            text: "Please, try another again."
        })

        setProductName("");
        setProductDescription("");
        setProductPrice("")
        }
      
      })
      .catch((error) => {
        console.error('Error creating product:', error);
        Swal.fire({
          title: "Invalid Input!",
          icon: "error",
          text: "Please, try another again."
      })
          setProductName("");
          setProductDescription("");
          setProductPrice("");
      });
  }

  // Function to retrieve all products
  function retrieveAllProducts() {
    fetch(`${process.env.REACT_APP_URL}/products/all`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`, // Add the JWT token to the Authorization header
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('All products:', data);
        setProducts(data); // Update the state with the fetched products
      })
      .catch((error) => {
        console.error('Error fetching products:', error);
      });
  }

  // Fetch all products on component mount
  useEffect(() => {
    retrieveAllProducts();
  }, []);


  return (
    <>
      <div className='home-background pt-5 mt-5'>
        <Container>
          <Row>
            <Col lg="6" md="7" sm="9" xs="12">
              <h1 className="home-header mb-5 mt-5">Create a Product</h1>
                <Form onSubmit={createProduct}>
                  <Form.Group className="mb-5 form-floating" controlId="productName">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control
                      type="text"
                      value={productName}
                      onChange={(e) => setProductName(e.target.value)}
                      placeholder="Name"
                    />
                  </Form.Group>
                  <Form.Group className="mb-5 form-floating" controlId="productDescription">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      type="text"
                      value={productDescription}
                      onChange={(e) => setProductDescription(e.target.value)}
                      placeholder="Product Description"
                    />
                  </Form.Group>
                  <Form.Group className="mb-5 form-floating" controlId="productPrice">
                    <Form.Label>Price</Form.Label>
                    <Form.Control
                      type="text"
                      value={productPrice}
                      onChange={(e) => setProductPrice(e.target.value)}
                      placeholder="Product Price"
                    />
                  </Form.Group>
                  {isActive ? (
                    <Button variant="danger" type="submit">
                      Add Product
                    </Button>
                  ) : (
                    <Button variant="danger" type="submit" disabled>
                      Add Product
                    </Button>
                  )}
                </Form>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );

 


}