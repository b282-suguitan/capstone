import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import React from 'react'
import { Container } from 'react-bootstrap';

export default function Home() {

	const data = {
		title: "Welcome Admin!",
		content: "You can now manage you store.",
		destination: "/dashboard",
		label: "View Dashboard"
	}


	return (
		<>
		<Container className='home-background' fluid>
		<Banner data={data} />
    	<Highlights />
    	</Container>
		</>
	)
}


