import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import React from 'react'
import 'animate.css';

export default function Home() {

	const data = {
		title: "Welcome to Flight Bag Fashions!",
		content: "Where fashion meets flight!",
		destination: "/products",
		label: "View Products"
	}


	return (
		<>
		<Container className='home-background animate__tada' fluid>
		<Banner data={data} />
    	<Highlights />
    	</Container>
		</>
	)
}


