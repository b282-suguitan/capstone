import { useEffect, useState, useContext} from "react";
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import '../App.css'


export default function Login(){
    const  navigate = useNavigate();
    const {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(false)


    useEffect(() => {
        if((email !== '' && password !== '')){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])


    // Log in
    function authenticate(e) {

        e.preventDefault()

            fetch(`${process.env.REACT_APP_URL}/users/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                
                if (typeof data.access !== "undefined") {
                    localStorage.setItem('token', data.access);
        
                    retrieveUserDetails(data.access);

                    // Alert
                    let timerInterval
                    Swal.fire({
                      title: 'Logging In',
                      html: 'Loading . . .<b></b>',
                      icon: 'success',
                      timer: 1500,
                      timerProgressBar: true,
                      didOpen: () => {
                        Swal.showLoading()
                        const b = Swal.getHtmlContainer().querySelector('b')
                        timerInterval = setInterval(() => {
                          b.textContent = Swal.getTimerLeft()
                        }, 100)
                      },
                      willClose: () => {
                        clearInterval(timerInterval)
                      }

                    }).then((result) => {
                     
                      if (result.dismiss === Swal.DismissReason.timer) {
                        console.log('I was closed by the timer')
                      }
                    })
                    
                    if (user?.isAdmin) {
                        navigate("/admin");
                    } else {
                        navigate("/");
                    }
              
                } else {
                    Swal.fire({
                        title: "Login Failed!",
                        icon: "error",
                        text: "Please re-enter your email or password."
                    });
                };
            })
        
            // Clears inout data
            setEmail('');
            setPassword('');

    }

    // Get token and pass to login
    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_URL}/users/details`,{
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

return (
    <>
      {user.id && user.isAdmin ? (
        <Navigate to="/admin" />
      ) : user.id ? (
        <Navigate to="/home" />
      ) : (

        <div className="login-background">
          <Container>
            <Row className="justify-content-center align-items-center min-vh-100">
              <Col lg="6" md="7" sm="9" xs="12">

                {/* FORMS */}
                <Form onSubmit={(e) => authenticate(e)}>
                  <h1 className="login-header">Login</h1>
                  <Form.Group className="mb-3 form-floating" controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                      type="email"
                      placeholder="Enter email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      required
                    />
                  </Form.Group>
          
                  <Form.Group className="mb-3 form-floating" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      type="password"
                      placeholder="Password"
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      required
                    />
                  </Form.Group>

                  {/* BUTTON */}
                  <div className="button-container">
                    {isActive ? (
                      <Button variant="danger" type="submit" id="submitBtn" className="btn-custom">
                        <p>Login</p>
                        <span className="send">
                          <FontAwesomeIcon icon={faPaperPlane} />
                        </span>
                        <span className="send2">
                          <FontAwesomeIcon icon={faPaperPlane} />
                        </span>
                      </Button>
                      ) : (
                      <Button variant="danger" type="submit" id="submitBtn" className="btn-custom" disabled>
                        <p>Login</p>
                        <span className="send">
                          <FontAwesomeIcon icon={faPaperPlane} />
                        </span>
                      </Button>
                    )}
                      <p className='login-link'>Don't have an account?</p>
                      <Button variant="danger" type="submit" id="submitBtn" className="btn-custom" as={Link} to={'/'}>
                        <p>Register</p>
                          <span className="send">
                            <FontAwesomeIcon icon={faPaperPlane} />
                          </span>
                      </Button>                 
                  </div>
                </Form>
              </Col>
            </Row> 
          </Container>
        </div>
        )};
    </>
  );
};
