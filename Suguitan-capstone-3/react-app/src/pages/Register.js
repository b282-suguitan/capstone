import {useState, useEffect,useContext} from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import '../App.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import Swal from 'sweetalert2';

export default function Register() {

    const navigate = useNavigate();
    const {user} = useContext(UserContext);
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState(false);



    useEffect(() => {
        if(( email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
    
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password1, password2])



    function registerUser(e) {


        e.preventDefault();



    // Registration

        fetch(`${process.env.REACT_APP_URL}/users/register`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password1
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data) {
            
                setEmail("");
                setPassword1("");
                setPassword2("");

                Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "Welcome to Flight Bag Fashions!"
                })

                navigate("/login");

            } else {

                Swal.fire({
                    title: "Email already registered!",
                    icon: "error",
                    text: "Please, try another email."
                })

            }
        })
    }

    
 return (
    
        (user.id !== null)?
        <Navigate to ="/login" />
        :
        <div className='registration-background'>
            <Container>
                <Row className="justify-content-center align-items-center min-vh-100">
                    <Col lg="6" md="7" sm="9" xs="12">
                        <Form onSubmit={(e) => registerUser(e)} className="registration-form">
                            <h1 className="registration-header">Create your account</h1>

                            <Form.Group className="mb-3 form-floating" controlId="userEmail">
                                <Form.Control
                                    type="email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    placeholder=" "
                                    className={email ? "has-value" : ""}
                                />
                                <Form.Label>Email address</Form.Label>
                            </Form.Group>

                            <Form.Group className="mb-3 form-floating" controlId="password1">
                                <Form.Control
                                    type="password"
                                    value={password1}
                                    onChange={(e) => setPassword1(e.target.value)}
                                    placeholder=" "
                                    className={password1 ? "has-value" : ""}
                                />
                                <Form.Label>Password</Form.Label>
                            </Form.Group>

                            <Form.Group className="mb-3 form-floating" controlId="password2">
                                <Form.Control
                                    type="password"
                                    value={password2}
                                    onChange={(e) => setPassword2(e.target.value)}
                                    placeholder=" "
                                    className={password2 ? "has-value" : ""}
                                />
                                <Form.Label>Verify Password</Form.Label>
                            </Form.Group>

                            
                            <div className="button-container">
                                {isActive ? (
                                <Button variant="danger" type="submit" id="submitBtn" className="btn-custom">
                                    <p>Register</p>
                                    <span className="send">
                                    <FontAwesomeIcon icon={faPaperPlane} />
                                    </span>
                                    <span className="send2">
                                    <FontAwesomeIcon icon={faPaperPlane} />
                                    </span>
                                </Button>
                                ) : (
                                <Button variant="danger" type="submit" id="submitBtn" className="btn-custom" disabled>
                                    <p>Register</p>
                                    <span className="send">
                                    <FontAwesomeIcon icon={faPaperPlane} />
                                    </span>
                                </Button>
                                )}
                                <p className='login-link'>Already have an account?</p>
                                <Button variant="danger" type="submit" id="submitBtn" className="btn-custom" as={Link} to='/login'>
                                    <p>Login</p>
                                    <span className="send">
                                    <FontAwesomeIcon icon={faPaperPlane} />
                                    </span>
                                </Button>
                            </div>
                         </Form>
                    </Col>
                </Row>    
            </Container>
        </div>
    );
};