import Banner from '../components/Banner';
import { Container } from 'react-bootstrap';

export default function Error() {

    const data = {
        title: "Error 404 - Page not found!",
        content: "The page you are looking for cannot be found.",
        destination: "/",
        label: "Back to Home"
    }

    return (
        <Container className='home-background' fluid>
        <Banner data={data} />
        </Container>
    )
}


