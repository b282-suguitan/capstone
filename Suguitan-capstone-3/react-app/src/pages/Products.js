import ProductCard from '../components/ProductCard';
import {useState, useEffect} from 'react';

export default function Product() {

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			setProducts(data.map(product => {
				return (
					<ProductCard key={product.id} product = {product} />
				)
			}))
		})
	}, []);


	return (
		<>
		{products}
		</>
	)
};