import React, { useState, useEffect } from 'react';
import {Button, Container, Col, Row, Form } from 'react-bootstrap'; // Make sure to import Card and Button from react-bootstrap
import 'animate.css';
import '../App.css'
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';

export default function Update() {
  const [productId, setProductId] = useState(''); // New state for product ID search
  const [productName, setProductName] = useState('');
  const [productDescription, setProductDescription] = useState('');
  const [productPrice, setProductPrice] = useState('');
  const [, setProducts] = useState([]);
  const [isActive, setIsActive] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    if( productId !== "" && (productName !== "" && productDescription !=="" && productPrice !=="")) {

        setIsActive(true)
    } else {
        setIsActive(false)
    }
}, [productId, productName, productDescription, productPrice])

 
function updateProduct(e) {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_URL}/products/update`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        _id: productId, // Pass the product ID for updating a specific product
        name: productName,
        description: productDescription,
        price: productPrice
      })
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('Product updated:', data);
        // Perform any additional actions if needed after successful product update
        if (!false) {
            setProductId("");
            setProductName("");
            setProductDescription("");
            setProductPrice("");
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Product Updated!',
                showConfirmButton: false,
                timer: 1500
              })
            navigate("/dashboard")
        } else {
            Swal.fire({
                title: "Invalid Input!",
                icon: "error",
                text: "Please, try another again."
            })
        }
      })
      .catch((error) => {
        console.error('Error updating product:', error);
        console.log('Response:', error.response); // Log the response
        Swal.fire({
            title: "Invalid Input!",
            icon: "error",
            text: "Please, try another again."
        })
            setProductName("");
            setProductDescription("");
            setProductPrice("");
    });
      
  }


  // Function to retrieve all products
  function retrieveAllProducts() {
    fetch(`${process.env.REACT_APP_URL}/products/all`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`, // Add the JWT token to the Authorization header
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('All products:', data);
        setProducts(data); // Update the state with the fetched products
      })
      .catch((error) => {
        console.error('Error fetching products:', error);
      });
  }

  // Fetch all products on component mount
  useEffect(() => {
    retrieveAllProducts();
  }, []);  

  return(
    <>
     <div className='home-background pt-5 mt-5'>
      <Container>
          <Row>
              <Col lg="6" md="7" sm="9" xs="12">
                    <h1 className="home-header mb-5 mt-5">Update Product</h1>
                      <Form onSubmit={updateProduct}>
                          <Form.Group className="mb-5">
                            <Form.Control
                                type="text"
                                value={productId}
                                onChange={(e) => setProductId(e.target.value)} // Handle input for product ID search
                                placeholder="Product ID"
                            />
                          </Form.Group>

                          <Form.Group className="mb-5">
                            <Form.Control
                                type="text"
                                value={productName}
                                onChange={(e) => setProductName(e.target.value)}
                                placeholder="Product Name"
                            />
                          </Form.Group>

                          <Form.Group className="mb-5">
                          <Form.Control
                              type="text"
                              value={productDescription}
                              onChange={(e) => setProductDescription(e.target.value)}
                              placeholder="Product Description"
                          />
                          </Form.Group>

                            <Form.Group className="mb-5">
                            <Form.Control
                                type="text"
                                value={productPrice}
                                onChange={(e) => setProductPrice(e.target.value)}
                                placeholder="Product Price"
                            />
                          </Form.Group>

                          {isActive ? (
                            <Button variant="danger" type="submit">
                              Update Product
                            </Button>
                          ) : (
                            <Button variant="danger" type="submit" disabled>
                              Update Product
                            </Button>
                          )}
                    </Form>
              </Col>
          </Row>
      </Container>
      </div>  
      </> 
  );
}