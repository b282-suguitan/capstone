import React, { useState, useEffect } from 'react';
import { Card, Button, Container, Col, Row } from 'react-bootstrap'; // Make sure to import Card and Button from react-bootstrap
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Dashboard() {
 
  const [products, setProducts] = useState([]);

  

   // Function to retrieve all products
   function retrieveAllProducts() {
    fetch(`${process.env.REACT_APP_URL}/products/all`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`, // Add the JWT token to the Authorization header
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('All products:', data);
        setProducts(data); // Update the state with the fetched products
      })
      .catch((error) => {
        console.error('Error fetching products:', error);
      });
  }

  // Fetch all products on component mount
  useEffect(() => {
    retrieveAllProducts();
  }, []);



  
  
  function archiveProduct(productId) {
    fetch(`${process.env.REACT_APP_URL}/products/archive/${productId}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        isActive: false
      })
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('Product archived:', data);

         
        // Update the product list state to reflect the change in isActive status
          setProducts((prevProducts) =>
          prevProducts.map((product) =>
            product._id === productId ? { ...product, isActive: false } : product
          )
        );
       
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Product is now out of stock!',
            showConfirmButton: false,
            timer: 1500
          })
        
      })
      .catch((error) => {
        console.error('Error archiving product:', error);
      });
  }

  function activateProduct(productId) {
    fetch(`${process.env.REACT_APP_URL}/products/activate/${productId}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        isActive: true
      })
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('Product activated:', data);
        // Update the product list state to reflect the change in isActive status
        setProducts((prevProducts) =>
          prevProducts.map((product) =>
            product._id === productId ? { ...product, isActive: true } : product
          )
        );

        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Product is now available!',
          showConfirmButton: false,
          timer: 1500
        })
      })
      .catch((error) => {
        console.error('Error activating product:', error);
      });
  }
    

  return (
    <>
    
    

      <Container fluid className='home-background pt-5 mt-5'>
        <Row>
          <Col lg={{ span: 5, offset: 2 }} md={{ span: 5, offset: 2 }} sm={{ span: 5, offset: 1 }} className='mt-4 mb-5 pb-5' >
              <h1 className='home-header pb-4'>Flight Fashions Product List</h1>
                <div>
                      {products.map((product) => (
                        <Card key={product._id} className="mb-3 glass-effect" >
                          <Card.Body>
                            <Card.Title>{product.name}</Card.Title>
                            <Card.Text>Description: {product.description}</Card.Text>
                            <Card.Text>Price: {product.price}</Card.Text>
                            <Card.Text>Status: {product.isActive ? 'Available' : 'Out of stock'}</Card.Text>
                            {product.isActive ? (
                              <Button variant="danger" onClick={() => archiveProduct(product._id)}>
                                Out of stock
                              </Button>
                            ) : (
                              <Button variant="danger" onClick={() => activateProduct(product._id)}>
                                Available
                              </Button>
                            )}
                            <Button className='mx-3' variant="danger" as={Link} to="/create">
                                Add product
                              </Button>
                              <Button variant="danger" as={Link} to="/update">
                                Edit product
                              </Button>
                            {/* You can add other details or buttons related to each product */}
                          </Card.Body>
                        </Card>
                      ))}
                </div>
          </Col>
        </Row>
      </Container>
      

    </>
  );
}

