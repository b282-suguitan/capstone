import {Row, Col, Carousel, Container} from 'react-bootstrap';


export default function Highlights() {
	return (
	
	<>	
		<Container>
			<div>
				<Row>
					<Col lg={5} md={8}>
						<Carousel >
							<Carousel.Item interval={2000}>
								<img 
									className='d-block custom-image'
									src= "./image-carousel/1.webp"
									alt= "1st slide"
									/>
							</Carousel.Item>
							
							<Carousel.Item interval={2000}>
								<img 
									className='d-block custom-image'
									src= "./image-carousel/2.webp"
									alt= "2nd slide"
									/>
							</Carousel.Item>
							<Carousel.Item interval={2000}>
								<img 
									className='d-block custom-image'
									src= "./image-carousel/3.webp"
									alt= "3rd slide"
									/>
							</Carousel.Item>
							<Carousel.Item interval={2000}>
								<img 
									className='d-block custom-image'
									src= "./image-carousel/4.webp"
									alt= "4th slide"
									/>
							</Carousel.Item>
							<Carousel.Item interval={2000}>
								<img 
									className='d-block custom-image'
									src= "./image-carousel/5.jpeg"
									alt= "5th slide"
									/>
							</Carousel.Item>
							<Carousel.Item interval={2000}>
								<img 
									className='d-block custom-image'
									src= "./image-carousel/6.webp"
									alt= "6th slide"
									/>
							</Carousel.Item>
						</Carousel>
					</Col>
				</Row>		
			</div>
		</Container>
	  </>
	)
}
