import { Button, Row, Col, Card, Container } from 'react-bootstrap';

import { Link } from 'react-router-dom';


export default function ProductCard({product}) {

const {name, description, price, _id} = product;


return (
    <>
<Container>
  <Row className='justify-content-center align-items-center'>
    <Col lg={6} md={6} sm={9}>
      <Card className='text-center glass-effect mt-5 mb-3'>
        <Card.Body>
          <Card.Title><h4>{name}</h4></Card.Title>
          <Card.Subtitle>Description</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price</Card.Subtitle>
          <Card.Text>{price}</Card.Text>
          <Button className="bg-danger" as={Link} to={`/products/${_id}`}>
          Proceed to checkout
          </Button>
        </Card.Body>
      </Card>
    </Col>
  </Row> 
</Container>
</>
    )
}