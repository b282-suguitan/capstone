import { useContext, useState, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {

    const {user} = useContext(UserContext);
    const navigate = useNavigate();
    // useParams() is a hook that will allow usto retrieve courseId passed via URL params
    const {productId} = useParams();
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);


	// Checkout
	const checkout = (productId) => {
		const token = localStorage.getItem('token');
		console.log(token);
			fetch(`${process.env.REACT_APP_URL}/users/checkout`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				products: [
				{
					productId: productId,
				}
				]
			})
			})
			.then(res => res.json())
			.then(data => { 
				console.log(data);
		
				if (data === true) {
					Swal.fire({
					title: "Checkout Successfull!",
					icon: "success",
					text: "Enjoy your new product"
					});
			
					navigate("/products");
				} else {
					Swal.fire({
					title: "You are an admin",
					icon: "error",
					text: "Only users can checkout."
					});
				}
				})
				.catch(error => {
				console.error("Error during checkout:", error);
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Something went wrong during checkout. Please try again later."
				});
			});
	  };
	
		// Increase quantity
		const handleIncreaseQuantity = () => {
			if (quantity < 3) {
			setQuantity(prevQuantity => prevQuantity + 1);
			} else {
			// Display an alert when the user tries to order more than 3
			Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: 'You can only checkout maximum of 3 items!'
			})
			}
		};
		
		// Decrease Quantity
		const handleDecreaseQuantity = () => {
			if (quantity > 1) {
			setQuantity(prevQuantity => prevQuantity - 1);
			}
		};
		
		// Calculate total amount
		const calculateTotalAmount = () => {
			return price * quantity;
		};

		useEffect(() => {
			// Fetch product details from the backend using productId
			fetch(`${process.env.REACT_APP_URL}/products/${productId}`)
			.then(res => res.json())
			.then(data => {
				console.log(data);
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
			});
		}, [productId]);
		
		return (
			<Container fluid className='home-background'>
				<h1 className='home-header mt-5 pt-5 mx-5'>Flight Bag Checkout</h1>
					<Row className='align-items-center justify-content-start'>
						<Col lg={{ span: 7, offset: 2 }} md={6} sm={9} className='mt-2' >
							<Card style={{ width: '45%'}} className='bg-transparent'>
								<Card.Img variant="top" src="../image-carousel/2.webp" alt={name} />
									<Card.Body className="text-center">
										<Card.Title><h3 className="text-white">{name}</h3></Card.Title>
										<Card.Subtitle className="text-white mt-3">Description:</Card.Subtitle>
										<Card.Text className="text-white">{description}</Card.Text>
										<Card.Subtitle className="text-white">Price:</Card.Subtitle>
										<Card.Text className="text-white">PhP {price}</Card.Text>
										<Card.Text className="text-white">Total Amount: PhP {calculateTotalAmount()}</Card.Text>
									</Card.Body>
									{user.id !== null ? (
										<>
										<div className='pb-4 text-center'>
											<Button variant="outline-danger" onClick={handleDecreaseQuantity}>
											-
											</Button>
											<span className="mx-2 text-white">{quantity}</span>
											<Button variant="outline-danger" onClick={handleIncreaseQuantity}>
											+
											</Button>
										</div>
										
										<div className='pb-4 mb-5 text-center'>
							
										<Button variant="danger" onClick={() => checkout(productId)}>
											Checkout
										</Button>
										</div>
										
										</>
									) : (
										<div>
										<Button className="btn btn-danger" as={Link} to="/login">
											Log in to checkout
										</Button>
										<div className="mt-2">Please log in to checkout this product.</div>
										</div>
									)}
							</Card>
						</Col>
					</Row>
			</Container>
		);
	}