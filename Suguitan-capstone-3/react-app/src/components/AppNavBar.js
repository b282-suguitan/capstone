import { useContext } from 'react';
import { Link, NavLink, useLocation } from 'react-router-dom';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import UserContext from '../UserContext';

function AppNavbar() {
  const { user } = useContext(UserContext);
  const { pathname } = useLocation();
  const hideNavbarRoutes = ['/login', '/'];

  const shouldHideNavbar = hideNavbarRoutes.includes(pathname);
  const isAdmin = user?.isAdmin;

  return (
    <>
      {!shouldHideNavbar && (
        <Navbar bg="secondary" data-bs-theme="dark" expand="lg" className="bg-body-tertiary">
          <Navbar.Brand as={Link} to="/home" className='px-3'>
            <img
            alt="Brand Logo"
            src="./logo.png" // Replace with the actual path to your logo image
            width="40"
            height="30"
            className="d-inline-block align-top"
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              {user.id && (
                <>
                  {isAdmin ? (
                    <>
                      <Nav.Link as={NavLink} to="/admin">
                        Admin
                      </Nav.Link>
                      <Nav.Link as={NavLink} to="/dashboard">
                        Dashboard
                      </Nav.Link>
                    </>
                  ) : (
                    <Nav.Link as={NavLink} to="/home">
                      Home
                    </Nav.Link>
                  )}
                </>
              )}
              <Nav.Link as={NavLink} to="/products">
                Products
              </Nav.Link>

              {user.id ? (
                <>
                  <Nav.Link as={NavLink} to="/logout">
                    Logout
                  </Nav.Link>
                </>
              ) : (
                <>
                  <Nav.Link as={NavLink} to="/login">
                    Login
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/">
                    Register
                  </Nav.Link>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      )}
    </>
  );
}

export default AppNavbar;
