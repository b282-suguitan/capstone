import { Button, Row, Col, Container, } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

    const {title, content, destination, label} = data;

    return (
        <Container>
          <div className="jordan-jumbotron">
            <Row>
              <Col lg="11" md="8" className="p-5">
                <h1 className='home-header animate__animated animate__tada'>{title}</h1>
                <p className='animate__animated animate__tada'>{content}</p>
                <Button className="view-products-button animate__animated animate__tada" as={Link} to={destination}>
                  <span>{label}</span>
                  <i></i>
                </Button>
              </Col>
            </Row>
          </div>
        </Container>
      );
      
    
}