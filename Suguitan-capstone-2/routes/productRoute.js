// Product Routes


// Connections

const express = require("express");

const router = express.Router();

const productController = require("../controllers/productController");

const auth = require("../auth");



// Route for creating a product

router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.createProduct(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving products
router.get("/all", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.getAllProducts(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving active products
router.get("/active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving inactive products
router.get("/inactive", (req, res) => {
	productController.getInActiveProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving single product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a product
router.put("/update", auth.verify, (req, res,) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	}
	productController.updateProduct(req.body, data).then(resultFromController => res.send(resultFromController));
});


// Route for archiving a product
router.patch("/archive/:productId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});


// Route for activating a product
router.patch("/activate/:productId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.activateProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});















module.exports = router;