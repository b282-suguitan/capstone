// User Routes

// Connections
const express = require("express");

const router = express.Router();

const userController = require("../controllers/userController");

const auth = require("../auth");

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getUser({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});

// Route for Non-admin User checkout (Create Order)

router.post("/checkout", auth.verify, (req, res) => {
	const data = {
	  isAdmin: auth.decode(req.headers.authorization).isAdmin,
	  userId: auth.decode(req.headers.authorization).id,
	};
	const { products, totalAmount, orderId} = req.body;
	
	userController.checkout(data, products, totalAmount, orderId).then(resultFromController => res.send(resultFromController))
	  
  });
 
  

module.exports = router;