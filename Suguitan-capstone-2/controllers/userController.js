// Controllers

// Connections

const User = require("../models/User");
const Product = require("../models/Product");

// const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Controller for user registration

module.exports.registerUser = async (reqBody) => {

   try{

   const registered = await User.findOne({ email: reqBody.email });
    if (registered) {
      return false;
    } else {
      const newUser = new User({
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 12)
      }); 
      const savedUser = await newUser.save();
      return true;
    };
} catch (err) {
	return `Error! Please try again!`;
}

};


// Controller for user and admin authentication



module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		// User exists
		} else {
			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			// If the passwords match/result of the above code is true
			if (isPasswordCorrect) {
				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
				return { success: true, access: auth.createAccessToken(result) };
			// Passwords do not match
			} else {
				return false;
			};
		};
	});
};







// Controller for retrieving a user
module.exports.getUser = (data) => {
	return User.findById(data.userId).then(result => {
    if (result) {
      // User found, set password to empty string and return the result
      result.password = "";
      return result;
    } else {
      // User not found, return null or an appropriate value to indicate that
      return false;
    }
  });
	
  };



// Controller for non admin checkout

module.exports.checkout = async (data, products, orderId) => {
  try {
    if (data.isAdmin) {
      return false;
    }

    let isUserUpdated = await User.findById(data.userId);
    if (!isUserUpdated) {
      return false;
    }

    const orderedProducts = [];

    for (const product of products) {
      let isProductUpdated = await Product.findById(product.productId);
      if (!isProductUpdated) {
        continue; // Skip this product and proceed with the next one if the product is not found
      }

      // Save the specific product into the user's orderedProduct
      const productToCheckout = {
        productId: product.productId,
        quantity: product.quantity || 1, // If you have a quantity property in product, add it here; default to 1 if not provided
        // Add any other product details you want to save
      };
      orderedProducts.push(productToCheckout);

      // Save the user's orderId in the product's userOrders array
      isProductUpdated.userOrders.push({ userId: data.userId, orderId });
      await isProductUpdated.save();
    }

    // Add all ordered products to the user's orderedProduct array
    isUserUpdated.orderedProduct.push(...orderedProducts);
    await isUserUpdated.save();

    return true;
  } catch (err) {
    return `Error! Please try again!`;
  }
};