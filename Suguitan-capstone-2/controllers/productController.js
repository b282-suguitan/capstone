// Product Controllers


// Connections
const Product = require("../models/Product");
const User = require("../models/User");
const auth = require("../auth");

// Controller for creating a product

module.exports.createProduct = async (data) => {
    
	try{
	if (data.isAdmin) {
        let newProduct = new Product({
          name: data.product.name,
          description: data.product.description,
          price: data.product.price
        });
        const product = await newProduct.save();
        return product;
		
    } else {
        return false;
    };      
} catch (err) {
	return `Error! Please try again!`;
}

};



// Controller for retrieving all the courses
module.exports.getAllProducts = (data) => {
	try {
	if (data.isAdmin){
		return Product.find({}).then(result => {
			return result;
		});
	} else {
		return false;
	}	
	
} catch (err) {
	return `Error! Please try again!`;
}

};


// Controller for retrieving active products
module.exports.getActiveProducts = () => {
	try{
	return Product.find({isActive: true}).then(result => {
		return result;
	});
} catch (err) {
	return false;
}
};

// Controller for retrieving inactive products
module.exports.getInActiveProducts = async () => {
	try{
	let result = await Product.find({ isActive: false });
	return result;
} catch (err) {
	return false;
}
};

// Controller for retrieving specific product
module.exports.getProduct = (reqParams) => {
	  return Product.findById(reqParams.productId).then(result => {
		return result;
	  });
	};
	
  


// Controller for updating a product
module.exports.updateProduct = async (reqBody, data) => {
	try{
	
	const updatedProduct = {
	  name: reqBody.name,
	  description: reqBody.description,
	  price: reqBody.price
	};
	if (data.isAdmin){
	const result = await Product.findByIdAndUpdate(reqBody._id, updatedProduct);
	  return result;
	} else {
	  return false;
	}

} catch (err) {
	return false;
}
};


// Controller for archiving a product
module.exports.archiveProduct = async (reqParams, data) => {
	
	const archiveProduct = {
        isActive: false
      };
	  try {
      const result = await Product.findByIdAndUpdate(reqParams.productId, archiveProduct);
  
      if (data.isAdmin && result) {
        return result;
      } else {
        return 'Failed to archive the product! You must be an admin!';
      }} catch (err) {
		return false;
	}
};



// Controller for activating a product
module.exports.activateProduct = async (reqParams, data) => {
  
      const archiveProduct = {
        isActive: true
      };
	  try {
      const result = await Product.findByIdAndUpdate(reqParams.productId, archiveProduct);
  
      if (data.isAdmin && result) {
        return result;
      } else {
        return 'Failed to activate the product! You must be an admin!';
	} } catch (err) {
		return `Error! Please try again!`;
	}
}
