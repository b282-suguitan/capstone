
// Express connection
const express = require("express");

// Mongoose connection
const mongoose = require("mongoose");

// Backend to Frontend connection
const cors = require("cors");

// Route connections

// userRoute connection
const userRoute = require("./routes/userRoute");

// productRoute coonection
const productRoute =  require("./routes/productRoute");

// Stores the initialization of express
const app = express();

// MongoDB Atlas connection
mongoose.connect("mongodb+srv://brynsgtn:NeverTheMind17@cluster0wdc028-course-b.ub2fsds.mongodb.net/E-commerce-API",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// MongoDB local connection
mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

// Middlewares

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Access to backend application
app.use(cors());

// http://localhost:2000/users
app.use("/users", userRoute);


// http://localhost:2000/courses
app.use("/products", productRoute);

// Port no.
app.listen(process.env.PORT || 2000, () => console.log(`Now listening to port ${process.env.PORT || 2000}!`));