// Mongoose connection
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password: {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orderedProduct :	[
		{
			products : [
				{
					productId : {
						type : String,
						ref: 'Product',
						required : [true, "Product ID is required"]
					},
					productName : String,
					quantity : Number
					
				}
			],
			totalAmount : Number,
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	],
		totalAmount : {
			type: Number 
		},
		orderId : {
			type: String
		},	
		purchaseOn: {
			type: Date,
			// Deafult to current timestamp
			default: new Date()
		}
});


module.exports = mongoose.model("User", userSchema);