// require mongoose
const mongoose = require("mongoose");

// productSchema
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		// Required data
		required: [true, "Course name is required!"]
	},
	description: {
		type: String,
		// Required data
		required: [true, "Description is required!"]
	},
	price: {
		type: Number,
		// Required data
		required: [true, "Price is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		// Deafult to current timestamp
		default: new Date()
	},
	userOrders: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required"]
			},
			orderId: {
				type: String
			}

		}
	]
});


module.exports = mongoose.model("Product", productSchema);